package com.cucumberjvm.williamh.steps;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.*;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumberjvm.williamh.pages.BasePage;

public class PlaceBetSteps extends BasePage {
	
	  public PlaceBetSteps(WebDriver driver) {
		  super (driver);
	  }
	
	  private Map<String, String> pageElementToClassMap = new HashMap<>();
			  
	@Given("^I navigate to william hill home-page and login$")
	public void i_navigate_to_william_hill_home_page_and_login() throws Throwable {
	    WebDriver driver = new ChromeDriver();
	    driver.get("http://sports.williamhill.com/sr-admin-set-white-list-cookie.html");
//	    BasePage.Login(driver);
	   
	}

	@Given("^I select \"([^\"]*)\" event from the menu$")
	public void i_navigate_to_any_event(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Given("^I add the first active selection to the bet slip$")
	public void i_add_the_first_active_selection_to_the_bet_slip() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@When("^I place a \"([^\"]*)\" bet to my bet slip$")
	public void i_place_a_bet_to_my_bet_slip(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^my bet slip should contain the following values$")
	public void my_bet_slip_should_contain_the_following_values(DataTable arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
	    // E,K,V must be a scalar (String, Integer, Date, enum etc)
		
		Map<String, Double> vals = arg1.asMap(String.class, Double.class);
		vals.forEach((key, value) -> verifySlipContains(key, value));
	    throw new PendingException();
	}

	private void verifySlipContains(String key, Double value) {
		// TODO Auto-generated method stub
		
		 WebElement element = driver.findElement(By.id(".betslip-footer__total-stake-price"));

		Assert.assertEquals(value, element.getValue());
	}

	@Then("^my user balance should be (\\d+)\\.(\\d+) less$")
	public void my_user_balance_should_be_less(int arg1, int arg2) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}
}
