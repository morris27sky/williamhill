package com.cucumberjvm.williamh;

import cucumber.api.junit.*;
import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/files",  
		plugin = {"pretty", "html:target/cucumber"}
		)

public class CucumberRunnerTest {

}
