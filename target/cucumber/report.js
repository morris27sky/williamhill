$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("PlaceBet.feature");
formatter.feature({
  "line": 1,
  "name": "Place a sport bet",
  "description": "As a user\nIn order to verify the contents of my betting slip\nI want to be able to place a bet",
  "id": "place-a-sport-bet",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 6,
  "name": "Place a bet and verify my betting slip",
  "description": "",
  "id": "place-a-sport-bet;place-a-bet-and-verify-my-betting-slip",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 7,
  "name": "I navigate to william hill home-page and login",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I select \"\u003csport\u003e\" event from the menu",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I add the first active selection to the bet slip",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I place a \"0.05\" bet to my bet slip",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "my bet slip should contain the following values",
  "rows": [
    {
      "cells": [
        "Key",
        "Value"
      ],
      "line": 12
    },
    {
      "cells": [
        "To return",
        "0.09"
      ],
      "line": 13
    },
    {
      "cells": [
        "Total stake",
        "0.05"
      ],
      "line": 14
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 15,
  "name": "my user balance should be 0.05 less",
  "keyword": "And "
});
formatter.examples({
  "line": 18,
  "name": "",
  "description": "",
  "id": "place-a-sport-bet;place-a-bet-and-verify-my-betting-slip;",
  "rows": [
    {
      "cells": [
        "sport"
      ],
      "line": 19,
      "id": "place-a-sport-bet;place-a-bet-and-verify-my-betting-slip;;1"
    },
    {
      "cells": [
        "football"
      ],
      "line": 20,
      "id": "place-a-sport-bet;place-a-bet-and-verify-my-betting-slip;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 20,
  "name": "Place a bet and verify my betting slip",
  "description": "",
  "id": "place-a-sport-bet;place-a-bet-and-verify-my-betting-slip;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 7,
  "name": "I navigate to william hill home-page and login",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I select \"football\" event from the menu",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I add the first active selection to the bet slip",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I place a \"0.05\" bet to my bet slip",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "my bet slip should contain the following values",
  "rows": [
    {
      "cells": [
        "Key",
        "Value"
      ],
      "line": 12
    },
    {
      "cells": [
        "To return",
        "0.09"
      ],
      "line": 13
    },
    {
      "cells": [
        "Total stake",
        "0.05"
      ],
      "line": 14
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 15,
  "name": "my user balance should be 0.05 less",
  "keyword": "And "
});
formatter.match({
  "location": "PlaceBetSteps.i_navigate_to_william_hill_home_page_and_login()"
});
formatter.result({
  "duration": 177227744,
  "error_message": "org.picocontainer.injectors.AbstractInjector$UnsatisfiableDependenciesException: com.cucumberjvm.williamh.steps.PlaceBetSteps has unsatisfied dependency \u0027interface org.openqa.selenium.WebDriver\u0027 for constructor \u0027public com.cucumberjvm.williamh.steps.PlaceBetSteps(org.openqa.selenium.WebDriver)\u0027 from org.picocontainer.DefaultPicoContainer@210ab13f:1\u003c|\n\tat org.picocontainer.injectors.ConstructorInjector.getGreediestSatisfiableConstructor(ConstructorInjector.java:191)\n\tat org.picocontainer.injectors.ConstructorInjector.getGreediestSatisfiableConstructor(ConstructorInjector.java:110)\n\tat org.picocontainer.injectors.ConstructorInjector.access$100(ConstructorInjector.java:51)\n\tat org.picocontainer.injectors.ConstructorInjector$1.run(ConstructorInjector.java:331)\n\tat org.picocontainer.injectors.AbstractInjector$ThreadLocalCyclicDependencyGuard.observe(AbstractInjector.java:270)\n\tat org.picocontainer.injectors.ConstructorInjector.getComponentInstance(ConstructorInjector.java:364)\n\tat org.picocontainer.injectors.AbstractInjectionFactory$LifecycleAdapter.getComponentInstance(AbstractInjectionFactory.java:56)\n\tat org.picocontainer.behaviors.AbstractBehavior.getComponentInstance(AbstractBehavior.java:64)\n\tat org.picocontainer.behaviors.Stored.getComponentInstance(Stored.java:91)\n\tat org.picocontainer.DefaultPicoContainer.getInstance(DefaultPicoContainer.java:699)\n\tat org.picocontainer.DefaultPicoContainer.getComponent(DefaultPicoContainer.java:647)\n\tat org.picocontainer.DefaultPicoContainer.getComponent(DefaultPicoContainer.java:678)\n\tat cucumber.runtime.java.picocontainer.PicoFactory.getInstance(PicoFactory.java:40)\n\tat cucumber.runtime.java.JavaStepDefinition.execute(JavaStepDefinition.java:38)\n\tat cucumber.runtime.StepDefinitionMatch.runStep(StepDefinitionMatch.java:37)\n\tat cucumber.runtime.Runtime.runStep(Runtime.java:300)\n\tat cucumber.runtime.model.StepContainer.runStep(StepContainer.java:44)\n\tat cucumber.runtime.model.StepContainer.runSteps(StepContainer.java:39)\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:44)\n\tat cucumber.runtime.junit.ExecutionUnitRunner.run(ExecutionUnitRunner.java:102)\n\tat org.junit.runners.Suite.runChild(Suite.java:128)\n\tat org.junit.runners.Suite.runChild(Suite.java:27)\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\n\tat cucumber.runtime.junit.ExamplesRunner.run(ExamplesRunner.java:59)\n\tat org.junit.runners.Suite.runChild(Suite.java:128)\n\tat org.junit.runners.Suite.runChild(Suite.java:27)\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\n\tat cucumber.runtime.junit.ScenarioOutlineRunner.run(ScenarioOutlineRunner.java:53)\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:63)\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:18)\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\n\tat cucumber.runtime.junit.FeatureRunner.run(FeatureRunner.java:70)\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:95)\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:38)\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\n\tat cucumber.api.junit.Cucumber.run(Cucumber.java:100)\n\tat org.eclipse.jdt.internal.junit4.runner.JUnit4TestReference.run(JUnit4TestReference.java:86)\n\tat org.eclipse.jdt.internal.junit.runner.TestExecution.run(TestExecution.java:38)\n\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.runTests(RemoteTestRunner.java:459)\n\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.runTests(RemoteTestRunner.java:678)\n\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.run(RemoteTestRunner.java:382)\n\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.main(RemoteTestRunner.java:192)\n\tat ✽.Given I navigate to william hill home-page and login(PlaceBet.feature:7)\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "football",
      "offset": 10
    }
  ],
  "location": "PlaceBetSteps.i_navigate_to_any_event(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "PlaceBetSteps.i_add_the_first_active_selection_to_the_bet_slip()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "0.05",
      "offset": 11
    }
  ],
  "location": "PlaceBetSteps.i_place_a_bet_to_my_bet_slip(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "PlaceBetSteps.my_bet_slip_should_contain_the_following_values(DataTable)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "0",
      "offset": 26
    },
    {
      "val": "05",
      "offset": 28
    }
  ],
  "location": "PlaceBetSteps.my_user_balance_should_be_less(int,int)"
});
formatter.result({
  "status": "skipped"
});
});