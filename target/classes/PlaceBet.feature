Feature: Place a sport bet
	As a user
	In order to verify the contents of my betting slip
	I want to be able to place a bet
	
Scenario Outline: Place a bet and verify my betting slip
	Given I navigate to william hill home-page and login
	And I select "<sport>" event from the menu
	And I add the first active selection to the bet slip
	When I place a "0.05" bet to my bet slip
	Then my bet slip should contain the following values
		| Key 			| Value |
		| To return 	| 0.09  |
		| Total stake 	| 0.05 	|
	And my user balance should be 0.05 less	
	
	
	Examples:
    	|  sport     |
    	|  football  |
